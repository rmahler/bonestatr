# bonestatr

This package is intended to facilitate the process of choosing the best stature
reconstruction method for human skeletal populations in the case of the poor
preservation of bones. This is an R implementation of the procedure
described in:

Mahler, R. (2022), A formalized approach to choosing the best methods for
  reconstructing stature in the case of poorly preserved skeletal series,
  *Archaeometry* doi: https://doi.org/10.1111/arcm.12700

## Author and maintaier:

Robert Mahler <r.mahler@.uw.edu.pl>

## License:

MIT
