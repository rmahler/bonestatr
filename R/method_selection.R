#'	Calculate the height differences for each method and list them in tabular form.
#'
#'  \code{formulae_selection} returns a data frame ranking regression formulae
#'      given in the 'formulae' dataset. The rationale behind this ranking is given
#'      in:
#'      Mahler, R. (2022), A formalized approach to choosing the best methods for reconstructing stature in the case of poorly preserved skeletal series, Archaeometry. doi: https://doi.org/10.1111/arcm.12700
#'
#'	@param bone_measurements A data frame containing columns for the sex and
#'	    lengths of three long bones: humerus, radius, and femur.
#'	    'sex' - can contain single character only and should not be of a factor
#'	    type; use 'f' for female or 'm' for male;
#'	    columns 'humerus', 'radius', 'femur' - should contain bone lengths in
#'	    millimetres; it is advised to use mean values there.
#'	@param return_format Determines the format of the return value. Can be either 'raw' or 'tex'. Default is set to 'raw'.
#'	@return	If return_format = 'raw' this function returns a data frame.
#'	    If return_format = 'tex' this function returns a LaTeX table code.
#'	@examples
#'	df <- data.frame(sex = c("m","f","f","m","m","f","m","m","m","m","f"),
#'	                 humerus = c(309,317,283,318,NaN,286,325,291,293,NaN,289),
#'	                 radius = c(249,238,210,243,NaN,NaN,254.5,231,248,NaN,227),
#'	                 femur = c(410,425,390,430.5,NaN,NaN,444.0,396,456,386,394))
#'	formulae_selection(df)
#'	formulae_selection(df, "tex")
#'
#'	@importFrom dplyr %>%
#'	@export
formulae_selection <- function(bone_measurements, return_format = "raw") {

bones <- c("humerus", "radius", "femur")
bones_short <- c("humerus" = "H", "radius" = "R", "femur" = "Fem")
for(i in bones) {
  stature_raw <- stature_ind(bone_measurements[, i],
                             bone_measurements[, "sex"], i)
  new_colnames <- paste(colnames(stature_raw)[3:ncol(stature_raw)],
                        bones_short[i], sep = "_")
  colnames(stature_raw)[3:ncol(stature_raw)] <- new_colnames

  if(exists("statures")) {
    statures <- cbind(statures, stature_raw)
  } else {
    statures <- stature_raw
  }
}

  methods_kinds_all <- formulae %>%
    dplyr::distinct(formula, variant, sex)

for( i in c("f","m") ) {

  # select rows with particular sex verdict
  statures_one_sex <-
    which(statures$sex == i)
  # select methods for certain sex only
  methods_kinds <- methods_kinds_all %>%
    dplyr::filter(sex == i)

  for( j in 1:nrow(methods_kinds) ) {

    if (as.character(methods_kinds[j,"variant"]) == "") {
      column_root <- as.character(methods_kinds[j,"formula"])
    } else {
      column_root <-
        paste(as.character(methods_kinds[j,"formula"]),
              as.character(methods_kinds[j,"variant"]), sep="_")
    }

    # get numbers of columns containing stature calculated with
    # column_root method
    columns_not_to_mix <-
      which(grepl("_DD", colnames(statures)))
    columns_to_mix <-
      which(grepl(column_root, colnames(statures)))
    columns_to_mix <-
      columns_to_mix [! columns_to_mix %in% columns_not_to_mix]

    # combine numbers of columns for diff calculation
    pairs <- utils::combn(columns_to_mix,2)

    for( k in 1:ncol(pairs) ) {

      column1 <- colnames(statures)[ pairs[1,k] ]
      column2 <- colnames(statures)[ pairs[2,k] ]

      namepart1 <-
        substr(column1,
               nchar(column_root)+2,
               nchar(column1))

      namepart2 <-
        substr(column2,
               nchar(column_root)+2,
               nchar(column2))

      diff_col <-
        paste(column_root, namepart1,
              namepart2, "DD",
              sep = "_")

      # calculate DD (but not absolute - to maintain normal distributions in order to apply t-test)
      statures[statures_one_sex,diff_col] <-
        (as.numeric(statures[statures_one_sex,pairs[1,k]]) -
           as.numeric(statures[statures_one_sex,pairs[2,k]]))
    }
  }
}

# select useful columns only
DD_columns <- which(grepl("_DD", colnames(statures)))
stature_diffs <- statures %>%
  dplyr::select(sex,tidyselect::all_of(DD_columns))

# calculate means, standard deviation, median, range
diff_stats <- data.frame()
k <- 1
for( i in c("f","m") ) {

  stature_diffs_sex <- stature_diffs %>%
    dplyr::filter(sex==i)

  # calculate stats for all diffs
  diff_cols <- 2:ncol(stature_diffs)
  for( j in diff_cols ) {
    # checks whether there are diffs to calculate
    n_diffs <- sum(!is.na( stature_diffs_sex[,j] ))
    if( n_diffs > 0 ) {
      diff_stats[k, "sex"] <- i

      # method name and bones diff
      method_split <-
        strsplit( colnames(stature_diffs_sex)[j],"_" )[[1]]
      l <- length(method_split)
      diff_stats[k, "bones_diff"] <-
        paste(method_split[(l-2):(l-1)], collapse="_")
      diff_stats[k, "formula"] <-
        paste(method_split[1:(l-3)], collapse="_")

      diff_stats[k, "n"] <- n_diffs
      diff_stats[k, "mean"] <-
        base::mean( stature_diffs_sex[,j], na.rm=TRUE )
      diff_stats[k, "sd"] <-
        stats::sd( stature_diffs_sex[,j], na.rm=TRUE )
      diff_stats[k, "median"] <-
        stats::median( stature_diffs_sex[,j], na.rm=TRUE )

      diff_range <-
        range( stature_diffs_sex[,j] , na.rm=TRUE)
      diff_stats[k, "diff_min"] <- diff_range[1]
      diff_stats[k, "diff_max"] <- diff_range[2]

      # test difference of mean from 0 (one sample Wilcox or Mann-Whitney U test)
      diff_test <-
        #    t.test(stature_diffs_sex[,j], na.action="na.omit")
        stats::wilcox.test(stature_diffs_sex[,j], mu=0, na.action="na.omit")
      diff_stats[k, "V"] <- diff_test["statistic"]
      diff_stats[k, "p"] <- diff_test["p.value"]

      k <- k+1
    }
  }
}

# oredering formula
bones_diff_order_formula <-
  c("H_R","H_Fem","R_Fem")
# add sorting accessory column
diff_sort <-
  match(diff_stats$bones_diff,bones_diff_order_formula)
# sort
ds_t <-
  diff_stats[
    with( diff_stats,
          order( diff_stats$sex,
                 diff_sort,
                 abs(diff_stats$mean) ) ), ]

#
# print stat tables (one for each sex)
#

ds_t_all <- ds_t
# limit stats to particular set of bones only
set_of_bones <- c("H_R","H_Fem","R_Fem")
ds_t_all <-
  ds_t_all[ ds_t_all$bones_diff %in% set_of_bones, ]

# adjust p values
for( i in c("f","m") ) {
  for( j in set_of_bones ) {
    ds_t_all[ (ds_t_all$sex==i & ds_t_all$bones_diff==j), "p.adj" ] <-
      stats::p.adjust( ds_t_all[ (ds_t_all$sex==i & ds_t_all$bones_diff==j), "p" ], method = "BH" )
  }
}

dst_method_from <-
  c("trotter_black","ruff","pearson",
    "raxter","trotter_white",
    "trotter_asian","trotter_mexican")
dst_method_to <-
  c("Trotter (b)","Ruff","Pearson",
    "Raxter","Trotter (w)",
    "Trotter (a)","Trotter (m)")
dst_bones_diff_from <-
  c("H_R","H_Fem","R_Fem")
dst_bones_diff_to <-
  c("H--R","H--Fem","R--Fem")
dst_colnames_from <-
  c("formula","bones_diff","n","mean",
    "sd","median","diff_min",
    "diff_max","V","p","p.adj")
dst_colnames_to <-
  c("formula","pair","\\(n\\)","\\(\\bar{\\Delta}\\)",
    "\\(s\\)","\\(M\\)","\\(\\Delta_{min}\\)",
    "\\(\\Delta_{max}\\)","\\(V\\)","\\(p\\)","\\(p_{adj}\\)")

# translate rownames
ds_t_all$formula <-
  plyr::mapvalues( ds_t_all$formula,
             from = dst_method_from,
             to = dst_method_to )

ds_t_all$bones_diff <-
  plyr::mapvalues( ds_t_all$bones_diff,
             from = dst_bones_diff_from,
             to = dst_bones_diff_to )

# round values
ds_t_all[,5:(ncol(ds_t_all)-1)] <-
  round( ds_t_all[,5:(ncol(ds_t_all)-1)], 2 )

i <- ncol(ds_t_all)
ds_t_all[,(i-1):i] <- sapply( ds_t_all[,(i-1):i], format_p )
row.names(ds_t_all) <- 1:nrow(ds_t_all)

bold <- function(x) {paste('\\multicolumn{1}{c}{\\textbf{',x,'}}', sep ='')}

printed2 <- list()
for( i in c("f","m") ) {
  # get table for particular sex only
  ds_t_sex <- ds_t_all[which(ds_t_all$sex==i),]
  ds_t_sex$sex <- NULL

  # wrap remaining labels for print
  nRows = nrow(ds_t_sex)
  nLbs <- as.vector( table(ds_t_sex$bones_diff)[ unique(ds_t_sex$bones_diff) ] )
  seqAll = 1:nRows
  seqLbs = cumsum( utils::head( c(1, nLbs), -1 ) )

  ds_t_sex$bones_diff[ seqAll [! seqAll %in% seqLbs] ] <- ""
  ds_t_sex$bones_diff[ seqLbs ] <-
    paste0( "\\multirow{", nLbs, "}{1.5cm}{\\centering \\textbf{",
            ds_t_sex$bones_diff[seqLbs], "}}", sep="" )

  ds_t_sex$n[ seqAll [! seqAll %in% seqLbs] ] <- ""
  ds_t_sex$n[ seqLbs ] <-
    paste0( "\\multirow{", nLbs, "}{*}{",
            ds_t_sex$n[seqLbs], "}", sep="" )

  # translate colnames
  colnames(ds_t_sex) <-
    plyr::mapvalues( colnames(ds_t_sex),
               from = dst_colnames_from,
               to = dst_colnames_to )

  sex <- c("female","male")
  names(sex) <- c("f","m")
  t_caption = paste0("Statistics for the differences between stature estimations (\\(\\Delta\\)) calculated using all the selected methods and ordered according to absolute mean difference, for combination of Fem, H, R bones of  ", sex[[i]], " individuals taken two at a time without repetition. M \u2013 median; V \u2013 a Wilcoxon test statistic; \\(p_{adj}\\) \u2013 \\(p\\) value adjusted for multiple comparisons using Benjamini-Hochberg\u2019s correction")
  t_print <- xtable::xtable(ds_t_sex,
                    caption = t_caption,
                    label = paste("tab:methods_compare",i,sep="_"),
                    auto = TRUE)
  xtable::align(t_print) <- paste0("rccc", paste0( rep("r",ncol(ds_t_sex)-3), collapse=""))

  printed2[i] <-
    print(t_print, print.results = FALSE,
          include.colnames = TRUE, include.rownames = FALSE,
          sanitize.text.function = identity,
          hline.after=c( -1, 0, utils::tail(seqLbs-1,-1), nRows ),
          floating = TRUE,
          sanitize.colnames.function = bold,
          latex.environments = "center", caption.placement = "top",
          table.placement="!ht",
          booktabs = TRUE, size = "\\small")
}

  if(return_format == "raw") {
    return(ds_t_all)
  } else if(return_format == "tex") {
    return(printed2)
  }
}



format_p <- function(x) {
  x_str <- c()
  for(i in x) {
    if( is.na(i) ) {
      x_str_add <- ""
    } else if( i < 0.0001 ) {
      x_str_add <- formatC(i, digits=1, format="e")
    } else {
      x_str_add <- formatC(i, digits=4, format="f")
    }
    x_str <- c(x_str, x_str_add)
  }
  return(x_str)
}
